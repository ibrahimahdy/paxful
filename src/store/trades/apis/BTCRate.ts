import axios from 'axios';

const baseURL = 'https://api.coindesk.com/';

export default axios.create({
  baseURL,
});
