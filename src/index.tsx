import React from 'react';
import ReactDOM from 'react-dom';
import Pages from './pages';
import { Provider } from 'react-redux';
import store from './store';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
  <Provider store={store}>
    <Pages />
  </Provider>,
  document.querySelector('#root')
);
